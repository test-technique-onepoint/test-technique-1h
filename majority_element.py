'''Consigne
def majority_element_indexes(lst):
    Return a list of the indexes of the majority element.
    Majority element is the element that appears more than
    floor(n / 2) times.
    If there is no majority element, return []
    >>> majority_element_indexes([1, 1, 2])
    [0, 1]
    >>> majority_element_indexes([1, 2])
    []
    >>> majority_element_indexes([])
    []
'''
from math import floor

def majority_element_indexes(list_elements : list) -> list:
    """Function that takes a list of elements and returns a list with indexes of elements that are more present than the majority value.

    Natives functions:
        len(): Return the number of items in a container.
        enumerate(): Return an enumerate object.

    Imported methods:
        math.floor(x: SupportsFloat): Return the floor of x as an Integral. Could have been replaced by int()

    Args:
        list_elements (list): List of elements that we go throught in order to find indexes of the values that are more represented than the majority value.

    Returns:
        list: List of indexes of values that are more represented then the majority value.
    """
    majority_value = floor(len(list_elements) / 2)
    index_list = []
    for i, value in enumerate(list_elements):
        if list_elements.count(value) > majority_value:  # apparently it doesn't have the best performance but I don't want to import more librairies than necessary
            index_list.append(i)
    return index_list
