def add_it_up(number: int) -> int:
    """Add_it_up function that takes an integer and if it's an integer return the sum from zero to the input parameter.

    Natives functions:
        isinstance(): Return whether an object is an instance of a class or of a subclass thereof.
        abs(): Return the absolute value of the argument.

    Args:
        number (int): Number that we use make the sum.

    Returns:
        int: The sum from zero to the given number.
    """
    if isinstance(number, int) is False:
        return 0
    x, i = 0, 0
    while i != abs(number):
        i += 1
        x += i
    if number < 0:  # test if number was a negative number the result will be negative.
        x = -x
    return x
