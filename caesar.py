def caesar_cipher(message: str, n: int) -> str:
    """Take a string and for each lowercase alphabet character takes the character and shift it to the value of n.

    Natives functions:
        ord(): Return the Unicode code point for a one-character string.
        chr(): Return a Unicode string of one character with ordinal i; 0 <= i <= 0x10ffff.

    Args:
        message (str): Message that we want to encrypt with caesar cipher.
        n (int): Number of letter to shift for the cipher.

    Returns:
        str: The encrypted message.
    """
    new_message = ""
    for a_letter in message:
        if ord("a") <= ord(a_letter) <= ord("z"):
            new_message += chr((ord(a_letter) - ord("a") + n) % 26 + ord("a"))
        else:
            new_message += a_letter
    return new_message
